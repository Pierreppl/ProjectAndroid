package com.example.pierreppl.projectandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Created by Sophie on 12/03/2017.
 */

// Recherche pokemon celon son type
public class ResearchType extends Activity {
    public Spinner input;
    final String EXTRA_POKEMON = "research_Pokemon";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_type);

        input = (Spinner) findViewById(R.id.typePokemon);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.liste_types, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        input.setAdapter(adapter_type);

        final Button rearchButton = (Button) findViewById(R.id.buttonValiderType);

        // Récupère valeur selectionnée par l'utilisateur et l'envoie au MainActivity
        rearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResearchType.this, MainActivity.class);
                intent.putExtra("typeAffichage","parType");
                intent.putExtra(EXTRA_POKEMON, input.getSelectedItem().toString());
                startActivity(intent);

            }
        });
    }
}
