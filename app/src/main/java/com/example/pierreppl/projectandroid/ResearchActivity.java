package com.example.pierreppl.projectandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Sophie on 07/03/2017.
 */

// Recherche pokemon celon son nom
public class ResearchActivity extends Activity {
    final String EXTRA_POKEMON = "research_Pokemon";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research);

        final EditText pokemon = (EditText) findViewById(R.id.nomPokemon);
        final Button rearchButton = (Button) findViewById(R.id.buttonValider);

        // Récupère valeur rentrée par l'utilisateur et l'envoie au MainActivity
        rearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResearchActivity.this, MainActivity.class);
                intent.putExtra("typeAffichage","parPokemon");
                intent.putExtra(EXTRA_POKEMON, pokemon.getText().toString());
                startActivity(intent);

            }
        });
    }
}


