package com.example.pierreppl.projectandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class PokedexBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "pokedex.db";
    private static final String TABLE_POKEDEX = "table_pokedex";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NOM = "NOM";
    private static final int NUM_COL_NOM = 1;
    private static final String COL_DESCRIPTION = "DESCRIPTION";
    private static final int NUM_COL_DESCRIPTION = 2;
    private static final String COL_TYPE_PREMIER = "TYPE_PREMIER";
    private static final int NUM_COL_TYPE_PREMIER = 3;
    private static final String COL_TYPE_SECOND = "TYPE_SECOND";
    private static final int NUM_COL_TYPE_SECOND = 4;
    private static final String COL_IMAGE = "IMAGE";
    private static final int NUM_COL_IMAGE = 5;
    private SQLiteDatabase bdd;
    private baseSQLite maBaseSQLite;

    public PokedexBDD(Context context){
        //On va créer la BDD et sa table
        maBaseSQLite = new baseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        // On ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        // On ferme la BDD en écriture
        bdd.close();
    }

    // Getter
    public SQLiteDatabase getBDD(){
        return bdd;
    }

    // Ajouter un pokemon
    public long ajouterPokemon(Pokemon pokemon){
        ContentValues value = new ContentValues();
        value.put(COL_NOM, pokemon.getNom());
        value.put(COL_DESCRIPTION, pokemon.getDescription());
        value.put(COL_TYPE_PREMIER, pokemon.getPremierType());
        value.put(COL_TYPE_SECOND, pokemon.getSecondType());
        value.put(COL_IMAGE, pokemon.getImage());
        return bdd.insert(TABLE_POKEDEX, null, value);
    }

    // Modifier un pokemon
    public int modifierPokemon(int id, Pokemon pokemon){
        ContentValues value = new ContentValues();
        value.put(COL_NOM, pokemon.getNom());
        value.put(COL_DESCRIPTION, pokemon.getDescription());
        value.put(COL_TYPE_PREMIER, pokemon.getPremierType());
        value.put(COL_TYPE_SECOND, pokemon.getSecondType());
        value.put(COL_IMAGE, pokemon.getImage());
        return bdd.update(TABLE_POKEDEX, value, COL_ID+" = ?", new String[]{String.valueOf(id)});
    }

    // Supprimer un pokemon
    public int supprimerPokemon(int id){
        return bdd.delete(TABLE_POKEDEX,COL_ID+" = ?", new String[]{String.valueOf(id)});
    }

    // Obtenir la liste des pokemon dans la bdd
    public ArrayList<Pokemon> getListePokemon(){
        ArrayList<Pokemon> listePokemon = new ArrayList<Pokemon>();
        String[] colonnes = {COL_ID, COL_NOM, COL_DESCRIPTION, COL_TYPE_PREMIER, COL_TYPE_SECOND, COL_IMAGE};
        Cursor cursor = bdd.query(TABLE_POKEDEX, colonnes, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Pokemon pokemon = new Pokemon();
            pokemon.setId(cursor.getInt(NUM_COL_ID));
            pokemon.setNom(cursor.getString(NUM_COL_NOM));
            pokemon.setDescription(cursor.getString(NUM_COL_DESCRIPTION));
            pokemon.setPremierType(cursor.getString(NUM_COL_TYPE_PREMIER));
            pokemon.setSecondType(cursor.getString(NUM_COL_TYPE_SECOND));
            pokemon.setImage(cursor.getInt(NUM_COL_IMAGE));
            listePokemon.add(pokemon);
            cursor.moveToNext();
        }
        cursor.close();
        return listePokemon;
    }

    // Obtenir une liste de Pokemon par son nom proche
    public ArrayList<Pokemon> getPokemonByNom(String nom){
        ArrayList<Pokemon> listePokemon = new ArrayList<Pokemon>();
        String[] colonnes = {COL_ID, COL_NOM, COL_DESCRIPTION, COL_TYPE_PREMIER, COL_TYPE_SECOND, COL_IMAGE};
        Cursor cursor = bdd.query(TABLE_POKEDEX,colonnes,COL_NOM+" LIKE \'%"+ nom +"%\'", null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Pokemon pokemon = new Pokemon();
            pokemon.setId(cursor.getInt(NUM_COL_ID));
            pokemon.setNom(cursor.getString(NUM_COL_NOM));
            pokemon.setDescription(cursor.getString(NUM_COL_DESCRIPTION));
            pokemon.setPremierType(cursor.getString(NUM_COL_TYPE_PREMIER));
            pokemon.setSecondType(cursor.getString(NUM_COL_TYPE_SECOND));
            pokemon.setImage(cursor.getInt(NUM_COL_IMAGE));
            listePokemon.add(pokemon);
            cursor.moveToNext();
        }
        cursor.close();
        return listePokemon;
    }

    // Obtenir une liste de pokemon par son id
    public ArrayList<Pokemon> getPokemonById(Integer identifiant){
        ArrayList<Pokemon> listePokemon = new ArrayList<Pokemon>();
        String[] colonnes = {COL_ID, COL_NOM, COL_DESCRIPTION, COL_TYPE_PREMIER, COL_TYPE_SECOND, COL_IMAGE};
        Cursor cursor = bdd.query(TABLE_POKEDEX,colonnes,COL_ID+" = '"+ identifiant +"'", null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Pokemon pokemon = new Pokemon();
            pokemon.setId(cursor.getInt(NUM_COL_ID));
            pokemon.setNom(cursor.getString(NUM_COL_NOM));
            pokemon.setDescription(cursor.getString(NUM_COL_DESCRIPTION));
            pokemon.setPremierType(cursor.getString(NUM_COL_TYPE_PREMIER));
            pokemon.setSecondType(cursor.getString(NUM_COL_TYPE_SECOND));
            pokemon.setImage(cursor.getInt(NUM_COL_IMAGE));
            listePokemon.add(pokemon);
            cursor.moveToNext();
        }
        cursor.close();
        return listePokemon;
    }

    // Obtenir une liste de Pokemon par son type
    public ArrayList<Pokemon> getPokemonByType(String type){
        ArrayList<Pokemon> listePokemon = new ArrayList<Pokemon>();
        String[] colonnes = {COL_ID, COL_NOM, COL_DESCRIPTION, COL_TYPE_PREMIER, COL_TYPE_SECOND, COL_IMAGE};
        Cursor cursor = bdd.query(TABLE_POKEDEX,colonnes,COL_TYPE_PREMIER+" = '"+ type +"' or " + COL_TYPE_SECOND + " = '" + type + "'", null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Pokemon pokemon = new Pokemon();
            pokemon.setId(cursor.getInt(NUM_COL_ID));
            pokemon.setNom(cursor.getString(NUM_COL_NOM));
            pokemon.setDescription(cursor.getString(NUM_COL_DESCRIPTION));
            pokemon.setPremierType(cursor.getString(NUM_COL_TYPE_PREMIER));
            pokemon.setSecondType(cursor.getString(NUM_COL_TYPE_SECOND));
            pokemon.setImage(cursor.getInt(NUM_COL_IMAGE));
            listePokemon.add(pokemon);
            cursor.moveToNext();
        }
        cursor.close();
        return listePokemon;
    }
}
