package com.example.pierreppl.projectandroid;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;

public class FormulairePokemon extends AppCompatActivity {
    // Variables
    public Integer id;
    public String nom;
    public String description;
    public String premierType;
    public String secondType;
    public Integer image;

    public EditText inputId;
    public EditText inputNom;
    public Spinner inputType1;
    public Spinner inputType2;
    public EditText inputDescription;
    public Button inputSubmit;
    public Button inputImage;
    public String type;
    String valeurType1;
    String valeurType2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulaire_pokemon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialisation des champs
        inputId = (EditText) findViewById((R.id.inputNumeroPokedex));
        inputNom = (EditText) findViewById(R.id.inputNom);
        inputDescription = (EditText) findViewById(R.id.inputDescription);
        inputType1 = (Spinner) findViewById(R.id.inputType1);
        inputType2 = (Spinner) findViewById(R.id.inputType2);
        ArrayAdapter<CharSequence> adapter_type1 = ArrayAdapter.createFromResource(this,
                R.array.liste_types, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_type2 = ArrayAdapter.createFromResource(this,
                R.array.liste_types, android.R.layout.simple_spinner_item);
        adapter_type1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputType1.setAdapter(adapter_type1);
        adapter_type1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputType2.setAdapter(adapter_type2);
        inputSubmit = (Button) findViewById(R.id.inputSubmit);
        inputImage = (Button) findViewById(R.id.inputImage);


        // Initialisation du bundle
        Intent intent = getIntent();
        Bundle donnees = intent.getExtras();
        type = donnees.getString("type");

        // Initialisation des valeurs si modification, et mise en place du nom du bouton de validation en fonction du type
        if (type.equals("création")) {
            inputSubmit.setText("Créer le Pokemon");
        }
        else if(type.equals("modification")) {
            inputSubmit.setText("Modifier le Pokemon");
            id = donnees.getInt("id");
            nom = donnees.getString("nom");
            description = donnees.getString("description");
            premierType = donnees.getString("premierType");
            secondType = donnees.getString("secondType");
            image = donnees.getInt("image");

            //Toast.makeText(getBaseContext(), id, Toast.LENGTH_SHORT).show();
            //inputId.setText(id);
            inputNom.setText(nom);
            inputDescription.setText(description);
            System.out.println(inputType1);
            System.out.println(inputType2);
            // On regarde si le type est égale à la valeur d'un select dans le spinner, si c'est le cas on l'instancie à l'element i du spinner
            if (premierType != null){
                for (int i = 0; i < inputType1.getCount();i++){
                    if(premierType.equals(inputType1.getItemAtPosition(i).toString())){
                        inputType1.setSelection(i);
                    };
                }
            }
            else{
                // On instancie à 0 si rien trouvé
                inputType1.setSelection(0);
            }
            // de même
            if(secondType != null){
                for (int i = 0; i < inputType2.getCount();i++){
                    if(secondType.equals(inputType2.getItemAtPosition(i).toString())){
                        inputType2.setSelection(i);
                    };
                }
            }
            else{
                inputType2.setSelection(0);

            }
        }
        // OnClick sur le bouton pour l'image
        inputImage.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Veuillez renseigner le nom du Pokemon.", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, 1);
            }
        });

        // OnClick sur le bouton de validation
        inputSubmit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                valeurType1 = inputType1.getSelectedItem().toString();
                valeurType2 = inputType2.getSelectedItem().toString();

                if (type.equals("création")){
                    if(controlePokemon()){
                        sendAjouterPokemon();
                    }
                }
                else if(type.equals("modification")) {
                    if (controlePokemon()) {
                        sendModifierPokemon();
                    }
                }
            }
        });
    }

    // Controle pour la validation
    public boolean controlePokemon() {
        if(inputNom.getText().toString().equals("")){
            Toast.makeText(getBaseContext(), "Veuillez renseigner le nom du Pokemon.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(inputDescription.getText().toString().equals("")){
            Toast.makeText(getBaseContext(), "Veuillez renseigner la description du Pokemon.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(valeurType1.equals("") && valeurType2.equals("")){
            Toast.makeText(getBaseContext(), "Veuillez renseigner au minimum un type au Pokemon..", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(valeurType1.equals("") && !valeurType2.equals("")){
            valeurType1 = valeurType2;
            valeurType2 = "";
        }
        else if (valeurType1.equals(valeurType2)){
            valeurType2 = "";
        }
        return true;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
//            Uri selectedImage = data.getData();
//            String[] filePathColumn = { MediaStore.Images.Media.DATA };
//
//            Cursor cursor = getContentResolver().query(selectedImage,
//                    filePathColumn, null, null, null);
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            String picturePath = cursor.getString(columnIndex);
//            cursor.close();
//
//            Bitmap bmp = null;
//            try {
//                bmp = getBitmapFromUri(selectedImage);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    // Envoi de l'ajout à MainActivity
    public void sendAjouterPokemon(){
        Intent MainActivity = new Intent(this, MainActivity.class);
        MainActivity.putExtra("nom",inputNom.getText().toString());
        MainActivity.putExtra("description",inputDescription.getText().toString());
        MainActivity.putExtra("premierType",valeurType1);
        MainActivity.putExtra("secondType",valeurType2);
        MainActivity.putExtra("image","image");
        setResult(RESULT_OK,MainActivity);
        finish();
    }

    // Envoi de la modif à MainActivity
    public void sendModifierPokemon(){
        Intent MainActivity = new Intent(this, MainActivity.class);
        MainActivity.putExtra("type","modification");
        MainActivity.putExtra("id",id);
        MainActivity.putExtra("nom",inputNom.getText().toString());
        MainActivity.putExtra("description",inputDescription.getText().toString());
        MainActivity.putExtra("premierType",valeurType1);
        MainActivity.putExtra("secondType",valeurType2);
        //MainActivity.putExtra("image","image");
        startActivity(MainActivity);
    }
}
