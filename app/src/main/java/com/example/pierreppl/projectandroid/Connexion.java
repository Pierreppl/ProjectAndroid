package com.example.pierreppl.projectandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Sophie on 11/03/2017.
 */

public class Connexion extends Activity{
    EditText champMotDePasse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);
        champMotDePasse = (EditText)findViewById(R.id.champMotDePasse);
    }

    // Methode création et vérification du mot de passe
    public void verificationMotDePasse(View v){
        FileOutputStream outputStream;
        FileInputStream inputStream;
        String filename = "password.txt";
        String password = champMotDePasse.getText().toString();
        try {
            inputStream = openFileInput(filename);
            int content;
            String motDePasseFile = "";
            while ((content = inputStream.read()) != -1) {
                motDePasseFile = motDePasseFile + Character.toString((char)content);
            }
            // Si mot de passe correct
            if(password.equals(motDePasseFile)){
                ((myApp) this.getApplication()).setConnect(true);

                Intent intent = new Intent(Connexion.this, MainActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(getBaseContext(), "Le mot de passe que vous avez renseigné n'est pas valide, veuillez réessayer.", Toast.LENGTH_SHORT).show();
            }
        } catch (FileNotFoundException e) {
            try{
                outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(password.getBytes());
                outputStream.close();
                Toast.makeText(getBaseContext(), "Le mot de passe a été défini, veuillez vous reconnecter.", Toast.LENGTH_SHORT).show();
            }
            catch (FileNotFoundException error) {
                Log.e("Message", "Le fichier n'existe pas.");
            }
            catch (Exception error) {
                Log.e("Message", "Impossible de définir le mot de passe.");
            }
        } catch (Exception e) {
            Log.e("Message", "Le file n'existe pas.");
        }
    }
}
