package com.example.pierreppl.projectandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView recyclerView;
    private MyPokedexAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Pokemon> pokedex;
    private ArrayList<Pokemon> pokemonFromBdd;
    public PokedexBDD bdd;
    public Integer RESULT_AJOUTER_POKEMON = 2;
    public Integer RESULT_MODIFIER_POKEMON = 3;
    public Integer RESULT_SUPPRIMER_POKEMON = 4;
    public String type;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //pokedex = new ArrayList<Pokemon>();

        // Déclaration de la base de données
        bdd = new PokedexBDD(this);
//        Pokemon pokemon = new Pokemon("Pikachu","test","Feu","Eau",R.mipmap.ic_launcher);
//        Pokemon pokemon1 = new Pokemon("Pikachu","test","Feu","Eau",R.mipmap.pokeball);
//        Pokemon pokemon2 = new Pokemon("Pikachu","test","Feu","Eau",R.mipmap.pokemon);
//        Pokemon pokemon3 = new Pokemon("Pikachu","test","Feu","Eau",R.mipmap.pokemon3);

        // Initialisation du bundle
        Intent intent = getIntent();
        Bundle donnees = intent.getExtras();
        type = "affichage";
        if(donnees != null){
            //Toast.makeText(getBaseContext(), donnees.getString("typeAffichage"), Toast.LENGTH_SHORT).show();
            if((donnees.getString("typeAffichage")!= null)){
                type = donnees.getString("typeAffichage");
            }
            // Si le type est différent de null et qu'il est égale à la modification, on construit un pokemon et on le modifie en fonction de l'id récupérer dans la bdd
            if (donnees.getString("type") != null && donnees.getString("type").equals("modification")){
                Integer id = donnees.getInt("id");
                String nom = donnees.getString("nom");
                String description = donnees.getString("description");
                String premierType = donnees.getString("premierType");
                String secondType = donnees.getString("secondType");
                Integer image = R.mipmap.ic_launcher;
                Pokemon pokemon = new Pokemon(id, nom, description, premierType, secondType, image);
                bdd.open();
                modifierPokemon(bdd, pokemon, id);
                rafraichirPokedex(bdd);
                bdd.close();
            }
        }
        // Si recherche un pokemon par son nom
        if(type.equals("parPokemon")){
            bdd.open();
            researchPokemon(bdd, donnees.getString("research_Pokemon"));
            bdd.close();
        // Si recherche un pokemon par son type
        } else if(type.equals("parType")) {
            bdd.open();
            researchPokemonByType(bdd, donnees.getString("research_Pokemon"));
            bdd.close();
        // Simple affichage
        } else if (type.equals("affichage")){
            bdd.open();
            rafraichirPokedex(bdd);
            bdd.close();
            adapter = new MyPokedexAdapter(pokedex, this,((myApp) this.getApplication()).getConnect());
            recyclerView.setAdapter(adapter);
        }

        // Si connecté dé-affiche l'item connexion et affiche l'ajout et la deconnexion
        if (((myApp) this.getApplication()).getConnect()){
            navigationView.getMenu().findItem(R.id.nav_connexion).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_add).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_deconnexion).setVisible(true);
        }
    }

    // Mehtode pour la recherche d'un pokemon par son nom
    private void researchPokemon(PokedexBDD bdd, String research) {
        pokedex = bdd.getPokemonByNom(research);
        adapter = new MyPokedexAdapter(pokedex, this, ((myApp) this.getApplication()).getConnect());
        recyclerView.setAdapter(adapter);
    }

    // Methode pour la recherche de pokemon par type
    private void researchPokemonByType(PokedexBDD bdd, String research) {
        pokedex = bdd.getPokemonByType(research);
        adapter = new MyPokedexAdapter(pokedex, this, ((myApp) this.getApplication()).getConnect());
        recyclerView.setAdapter(adapter);
    }

    // Methode pour raffraichir au sein de la bdd
    private void rafraichirPokedex(PokedexBDD bdd) {
        pokedex = bdd.getListePokemon();
        adapter = new MyPokedexAdapter(pokedex, this, ((myApp) this.getApplication()).getConnect());
        recyclerView.setAdapter(adapter);
    }

    // Méthode pour ajouter au sein de la bdd
    private void ajouterPokemon(PokedexBDD bdd, Pokemon pokemon){
        bdd.ajouterPokemon(pokemon);
    }

    // Méthode pour modifier au sein de la bdd
    private void modifierPokemon(PokedexBDD bdd, Pokemon pokemon, int id){
        bdd.modifierPokemon(id, pokemon);
        rafraichirPokedex(bdd);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // Action du menu
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_pokedex) {
            bdd.open();
            rafraichirPokedex(bdd);
            bdd.close();
            adapter = new MyPokedexAdapter(pokedex, this, ((myApp) this.getApplication()).getConnect());
            recyclerView.setAdapter(adapter);
        } else if (id == R.id.nav_research) {
            Intent intent = new Intent(MainActivity.this, ResearchActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_type) {
            Intent intent = new Intent(MainActivity.this, ResearchType.class);
            startActivity(intent);
        } else if (id == R.id.nav_connexion) {
            Intent intent = new Intent(MainActivity.this, Connexion.class);
            startActivity(intent);
        } else if (id == R.id.nav_add) {
            Intent intent = new Intent(MainActivity.this, FormulairePokemon.class);
            intent.putExtra("type","création");
            startActivityForResult(intent,RESULT_AJOUTER_POKEMON);
        } else if (id == R.id.nav_deconnexion) {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.getMenu().findItem(R.id.nav_connexion).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_add).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_deconnexion).setVisible(false);

            ((myApp) this.getApplication()).setConnect(false);

            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_AJOUTER_POKEMON) {
            if (resultCode == RESULT_OK) {
                String nom = data.getStringExtra("nom");
                String description = data.getStringExtra("description");
                String premierType = data.getStringExtra("premierType");
                String secondType = data.getStringExtra("secondType");
                Pokemon pokemon = new Pokemon(nom, description, premierType, secondType, R.mipmap.pokeball);
                bdd.open();
                ajouterPokemon(bdd,pokemon);
                rafraichirPokedex(bdd);
                bdd.close();
            }
        }
        if(requestCode == RESULT_MODIFIER_POKEMON) {
            int id = data.getIntExtra("id",-1);
            String nom = data.getStringExtra("nom");
            String description = data.getStringExtra("description");
            String premierType = data.getStringExtra("premierType");
            String secondType = data.getStringExtra("secondType");
            Pokemon pokemon = new Pokemon(id, nom, description, premierType, secondType, R.mipmap.pokeball);
            bdd.open();
            modifierPokemon(bdd, pokemon, id);
            rafraichirPokedex(bdd);
            bdd.close();
        }
    }
}