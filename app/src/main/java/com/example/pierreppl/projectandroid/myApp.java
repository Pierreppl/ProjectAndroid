package com.example.pierreppl.projectandroid;

import android.app.Application;

/**
 * Created by Sophie on 11/03/2017.
 */

// Classe qui permet de gérer la connexion
public class myApp extends Application {
    private boolean connect;

    // Récupère statut de la connexion
    public boolean getConnect() {
        return connect;
    }

    // Change statut de la connexion
    public void setConnect(boolean bool){
        connect = bool;
    }
}
