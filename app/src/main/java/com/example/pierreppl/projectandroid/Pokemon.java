package com.example.pierreppl.projectandroid;

public class Pokemon {

    // Variables
    private int id;
    private String nom;
    private String description;
    private String premierType;
    private String secondType;
    private Integer image;

    // Constructeurs
    public Pokemon(){}

    public Pokemon(Integer id, String nom, String description, String premierType, String secondType, Integer image){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.premierType = premierType;
        this.secondType = secondType;
        this.image = image;
    }


    public Pokemon(String nom, String description, String premierType, String secondType, Integer image){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.premierType = premierType;
        this.secondType = secondType;
        this.image = image;
    }

    //Setters
    public void setId(int id){
        this.id = id;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setPremierType(String premierType){
        this.premierType = premierType;
    }

    public void setSecondType(String secondType){
        this.secondType = secondType;
    }

    public void setImage(Integer image){
        this.image = image;
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public String getPremierType() {
        return premierType;
    }

    public String getSecondType() {
        return secondType;
    }

    public Integer getImage() {
        return image;
    }

    public String toString(){
        return "identifiant : "+this.getId()+" nom : "+this.getNom()+" description : "+this.getDescription()+" type : "+this.getPremierType()+" "+this.getSecondType() +" image : "+this.getImage();
    }
}
