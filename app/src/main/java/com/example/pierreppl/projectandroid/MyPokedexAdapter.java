package com.example.pierreppl.projectandroid;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;



public class MyPokedexAdapter extends RecyclerView.Adapter<MyPokedexAdapter.PokedexViewHolder> {
    // variables
    private ArrayList<Pokemon> pokedex;
    public Context context;
    public boolean connecter;

    // Class statique pour initialiser les textes, images, boutons dans la cardview
    public class PokedexViewHolder extends RecyclerView.ViewHolder  {
        public CardView pokemon;
        public ImageView imagePokemon;
        public TextView nomPokemon;
        public TextView descriptionPokemon;
        public ImageButton boutonEdition;
        public ImageButton boutonSuppression;
        public PokedexViewHolder(View itemView) {
            super(itemView);
            pokemon = (CardView)itemView.findViewById(R.id.pokemon);
            imagePokemon = (ImageView) itemView.findViewById(R.id.imagePokemon);
            nomPokemon = (TextView)itemView.findViewById(R.id.nomPokemon);
            descriptionPokemon = (TextView)itemView.findViewById(R.id.descriptionPokemon);
            boutonEdition = (ImageButton) itemView.findViewById(R.id.boutonEdition);
            boutonSuppression = (ImageButton) itemView.findViewById(R.id.boutonSuppression);
            // Si connecté affiche les boutons d'édition et de suppression
            if (connecter){
                boutonEdition.setVisibility(View.VISIBLE);
                boutonSuppression.setVisibility(View.VISIBLE);
            }
        }

    }

    // Constructeur
    public MyPokedexAdapter(ArrayList<Pokemon> pokedex, Context context, boolean connecter) {
        this.pokedex = pokedex;
        this.context = context;
        this.connecter = connecter;
    }

    // Création d'une vue
    @Override
    public PokedexViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view,parent,false);
        PokedexViewHolder pokedexViewHolder = new PokedexViewHolder(view);
        return pokedexViewHolder;
    }

    // Gestion des clics, en fonction du clic on va récupérer les informations d'un pokemon, une cardview = 1 pokemon, 1 cardview = des interactions specifiques
    @Override
    public void onBindViewHolder(final PokedexViewHolder holder, final int position) {
        holder.imagePokemon.setImageResource(pokedex.get(position).getImage());
        holder.nomPokemon.setText(pokedex.get(position).getNom());
        holder.descriptionPokemon.setText(pokedex.get(position).getDescription());
        holder.pokemon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(view.getContext(),ConsulterPokemon.class);
                intent.putExtra("id",pokedex.get(position).getId());
                intent.putExtra("nom",pokedex.get(position).getNom());
                intent.putExtra("premierType",pokedex.get(position).getPremierType());
                intent.putExtra("secondType",pokedex.get(position).getSecondType());
                intent.putExtra("description",pokedex.get(position).getDescription());
                intent.putExtra("image",pokedex.get(position).getImage());

                view.getContext().startActivity(intent);
            }
        });
        // edition
        holder.boutonEdition.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(view.getContext(),FormulairePokemon.class);
                intent.putExtra("id",pokedex.get(position).getId());
                intent.putExtra("type","modification");
                intent.putExtra("nom",pokedex.get(position).getNom());
                intent.putExtra("premierType",pokedex.get(position).getPremierType());
                intent.putExtra("secondType",pokedex.get(position).getSecondType());
                intent.putExtra("description",pokedex.get(position).getDescription());
                intent.putExtra("image",pokedex.get(position).getImage());
                view.getContext().startActivity(intent);
            }
        });
        // suppression
        holder.boutonSuppression.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Context context = view.getContext();
                PokedexBDD bdd = new PokedexBDD(context);
                bdd.open();
                bdd.supprimerPokemon(pokedex.get(position).getId());
                bdd.close();
                Intent intent = new Intent(view.getContext(),MainActivity.class);
                intent.putExtra("id",pokedex.get(position).getId());
                intent.putExtra("type","suppression");
                view.getContext().startActivity(intent);
            }
        });
    }

    // Count
    @Override
    public int getItemCount() {
        return pokedex.size();
    }

    // Attachement à la recycler view
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
