package com.example.pierreppl.projectandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class baseSQLite extends SQLiteOpenHelper {
    // Declaration des variables pour la création de la BDD
    private static final String TABLE_POKEDEX = "table_pokedex";
    private static final String COL_ID = "ID";
    private static final String COL_NOM = "NOM";
    private static final String COL_DESCRIPTION = "DESCRIPTION";
    private static final String COL_TYPE_PREMIER = "TYPE_PREMIER";
    private static final String COL_TYPE_SECOND = "TYPE_SECOND";
    private static final String COL_IMAGE = "IMAGE";

    // Query complete pour la création de la BDD
    private static String CREATE_BDD = "CREATE TABLE " + TABLE_POKEDEX + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_NOM + " TEXT NOT NULL, "
            + COL_DESCRIPTION + " TEXT NOT NULL, "
            + COL_TYPE_PREMIER + " TEXT NOT NULL, "
            + COL_TYPE_SECOND + " TEXT NOT NULL, "
            + COL_IMAGE + " INTEGER NOT NULL);";

    // Constructeur
    public baseSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase bd){
        bd.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int oldVersion, int newVersion) {
//        bd.execSQL("DROP TABLE " + TABLE_POKEDEX + ";");
//        onCreate(bd);
    }
}
