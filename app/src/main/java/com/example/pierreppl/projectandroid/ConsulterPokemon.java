package com.example.pierreppl.projectandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ConsulterPokemon extends AppCompatActivity {
    // Variables
    public ImageView imagePokemon;
    public TextView idPokemon;
    public TextView nomPokemon;
    public TextView premierTypePokemon;
    public TextView secondTypePokemon;
    public TextView descriptionPokemon;

    public String identifiant;
    public String valeurPremierType;
    public String valeurSecondType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fiche_pokemon);

        imagePokemon = (ImageView) findViewById(R.id.imageFichePokemon);
        idPokemon = (TextView) findViewById(R.id.idFichePokemon);
        nomPokemon = (TextView) findViewById(R.id.nomFichePokemon);
        premierTypePokemon = (TextView) findViewById(R.id.premierTypePokemon);
        secondTypePokemon = (TextView) findViewById(R.id.secondTypePokemon);
        descriptionPokemon = (TextView) findViewById(R.id.descriptionFichePokemon);

        Intent intent = getIntent();
        Bundle donnees = intent.getExtras();

        imagePokemon.setImageResource(Integer.parseInt(donnees.get("image").toString()));
        //idPokemon.setText(idPokemon.getText()+donnees.get("identifiant").toString());
        nomPokemon.setText(donnees.get("nom").toString());
        premierTypePokemon.setText(donnees.get("premierType").toString());
        secondTypePokemon.setText(donnees.get("secondType").toString());
        descriptionPokemon.setText(donnees.get("description").toString());
        valeurPremierType = donnees.get("premierType").toString();
        valeurSecondType = donnees.get("secondType").toString();
        setCouleurType(premierTypePokemon, valeurPremierType);
        setCouleurType(secondTypePokemon, valeurSecondType);

    }

    // Mise en place de la couleur du type en fonction de la valeur du spinner
    public void setCouleurType(TextView textview, String valeur){
        if(valeur.equals("Acier")){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeAcier));
        }
        else if(valeur.equals(("Combat"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeCombat));
        }
        else if(valeur.equals(("Dragon"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeDragon));
        }
        else if(valeur.equals(("Eau"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeEau));
        }
        else if(valeur.equals(("Electrique"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeElectrique));
        }
        else if(valeur.equals(("Fée"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeFee));
        }
        else if(valeur.equals(("Feu"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeFeu));
        }
        else if(valeur.equals(("Glace"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeGlace));
        }
        else if(valeur.equals(("Insecte"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeInsecte));
        }
        else if(valeur.equals(("Normal"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeNormal));
        }
        else if(valeur.equals(("Plante"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typePlante));
        }
        else if(valeur.equals(("Poison"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typePoison));
        }
        else if(valeur.equals(("Psy"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typePsy));
        }
        else if(valeur.equals(("Sol"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeSol));
        }
        else if(valeur.equals(("Spectre"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeSpectre));
        }
        else if(valeur.equals(("Ténèbres"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeTenebres));
        }
        else if(valeur.equals(("Vol"))){
            textview.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.typeVol));
        }
    }

}
